# Poduptime

Poduptime is PHP software to observe stats and data on servers that support nodeinfo over the fediverse from projects like Mastoson, Diaspora, Friendica, Pixelfed...

# Translation Help
We can always use your help with languages
https://translate.diasp.org/

# Installation

Environmental items you need (debian based system sample):

```
php8.4-{common,curl,pgsql,zip,cli,fpm,bcmath,readline,mbstring,xml,intl,yaml} curl postgresql-17 postgresql-contrib postgresql-17-postgis dnsutils npm nodejs gzip pigz
```
In postgres:  
CREATE EXTENSION Postgis;  

Yarn is a separate install: https://yarnpkg.com/getting-started/install  
Composer is a separate install: https://getcomposer.org/  
GeoIP database setup: https://github.com/maxmind/geoipupdate  

Clone and setup:
```
git clone https://gitlab.com/diasporg/poduptime
cd poduptime
yarn install
composer install
cp .env.example .env 
```
Edit `.env` to update your DB and file settings  
Your postgres user needs superuser rights, then:
```
php vendor/bin/phinx seed:run
php vendor/bin/phinx migrate
```

# Usage
***Backend***  
Main script:  
run `bin/console updateall` to update data for all servers  
run `bin/console` for more admin commands to update and maintain 

***Frontend***  
Point your favorite webserver to index.php

# Upgrading
`bin/console upgrade`  

# Status

[![pipeline status](https://gitlab.com/diasporg/poduptime/badges/develop/pipeline.svg)](https://gitlab.com/diasporg/poduptime/-/commits/develop)
[![coverage report](https://gitlab.com/diasporg/poduptime/badges/develop/coverage.svg)](https://gitlab.com/diasporg/poduptime/-/commits/develop)
[![Crowdin](https://badges.crowdin.net/poduptime/localized.svg)](https://crowdin.com/project/poduptime)

============================

Source for Poduptime

  Poduptime is software to get live stats and data on federated network servers.
  Copyright (C) 2011 David Morley

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
