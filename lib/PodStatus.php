<?php

/**
 * Pod status types enum.
 */

declare(strict_types=1);

namespace Poduptime;

use CommerceGuys\Enum\AbstractEnum;

final class PodStatus extends AbstractEnum
{
    public const DOWN           = 0;
    public const UP             = 1;
    public const RECHECK        = 2;
    public const PAUSED         = 3;
    public const SYSTEM_DELETED = 4;
    public const USER_DELETED   = 5;
}
