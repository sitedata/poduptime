<?php

/**
 * ping an API like friendica/mastodon
 */

declare(strict_types=1);

use Icamys\SitemapGenerator\Config;
use RedBeanPHP\R;
use Carbon\Carbon;
use Poduptime\UpdateServerTask;
use Spatie\Async\Pool;
use Poduptime\AddServerTask;
use RedBeanPHP\RedException;
use RedBeanPHP\RedException as RedExceptionAlias;
use Laminas\Feed\Writer\Feed;
use Icamys\SitemapGenerator\SitemapGenerator;

function pushUpdateAPI()
{
    $lastpush   = round(Carbon::createFromFormat('Y-m-d H:i:s.u', getMeta('pushed_update', 'date_created'))->diffInHours());
    $lastupdate = round(Carbon::createFromFormat('Y-m-d H:i:s.u', getMeta('pods_updated', 'date_created'))->diffInHours());
    $sql        = "SELECT domain FROM servers WHERE date_diedoff > current_date - interval '$lastpush hours'";
    $deadpods   = R::getAll($sql);

    $sql = "
    SELECT
        to_char(date_checked, 'yyyy-mm') AS yymm,
        total_users AS users,
        total_active_users_monthly AS monthly_users
    FROM monthlystats
    WHERE date_checked BETWEEN (now() - interval '2 month') and now() 
    AND softwarename = 'all'
    GROUP BY yymm, users, monthly_users
    ORDER BY yymm desc
    LIMIT 1
        ";
    $allusers = R::getAll($sql);

    $yesterday = date("Y-m-d", strtotime("yesterday")) . ' 01:01:01-01';
    $sql = "
    SELECT
        to_char(date_checked, 'yyyy-mm-dd') AS yymmdd,
        total_active_users_monthly AS monthly_users
    FROM dailystats
    WHERE date_checked = '$yesterday' AND softwarename = 'all'
    GROUP BY yymmdd, monthly_users
    ORDER BY yymmdd desc
    LIMIT 1
        ";
    $yesterdayssusers = R::getAll($sql);

    $servers        = allServersList(null, true, $lastpush);
    $count          = number_format(floatval(allDomainsData(null, true)[0]['count']));
    $ustot          = number_format(floatval($allusers[0]['users'] ?? 0));
    $yesterdaytotal = number_format(floatval($yesterdayssusers[0]["monthly_users"] ?? 0));
    $dp             = count($deadpods);
    $bq             = count($servers);

    $string = "Found {$bq} new servers and {$dp} servers died off since {$lastpush} hours ago.\n\n";
    $string .= "[url=https://" . $_SERVER['DOMAIN'] . "/status]{$count}[/url] servers checked. {$ustot} Total Users with {$yesterdaytotal} Active Users today. Check out the [url=https://" . $_SERVER['DOMAIN'] . "/stats]stats[/url]! \n\n";

    if ($bq > 0) {
        $string .= "New #fediverse servers found:\n\n";
    }

    foreach ($servers as $server) {
        $string .= "[url=https://{$server['softwarename']}." . $_SERVER['DOMAIN'] . "/{$server['domain']}]{$server['domain']}[/url] a #{$server['softwarename']} server from {$server['countryname']}\n";
    }

    $string .= "\n\nHelp others find a home, send them to [url=https://" . $_SERVER['DOMAIN'] . "]" . $_SERVER['DOMAIN'] . "[/url]";

    if (($bq > 0 || $dp > 0) && ($lastupdate < $lastpush)) {
        $conn = $_SERVER['PUSH_USER'] . ':' . $_SERVER['PUSH_PASS'] . ' ' . $_SERVER['PUSH_URL'];
        `curl -s -u $conn -d status="$string"`;
    }
    return "Push Done";
}

/**
 * @throws Exception
 */
function upateALL($dead = false)
{
    set_time_limit(0);
    if (getMeta('pods_updating')) {
        die('already running');
    }
    if (!isConnected()) {
        die('no internet');
    }
    $time_start = microtime(true);
    if (getMeta('pods_updating')) {
        die('already running');
    }
    addMeta('pods_updating', true);
    $servers = allDomainsData(null, false, $dead) ?? [];
    podLog('Updating All Servers', count($servers));
    $pool    = Pool::create()->concurrency((int) $_SERVER['PROCESS_LIMIT']);
    $processed = 0;
    foreach (array_column($servers, 'domain') as $domain) {
        $pool
            ->add(new UpdateServerTask($domain))
            ->catch(function ($exception) use ($domain) {
                podLog('error on updating ' . $exception, $domain, 'error');
            });
        $processed++;
    }
    $pool->wait();
    podLog('Updated All Servers', $processed);
    gc_collect_cycles();
    addMeta('pods_updated');
    $time_end       = microtime(true);
    $execution_time = ($time_end - $time_start) / 60;
    addMeta('pods_update_runtime', round($execution_time));
    addMeta('pods_updating', false);
    return "All Servers Updated";
}

function crawlFediverse($limit = null): string
{
    set_time_limit(0);
    $existingPods  = allDomainsData(null, false, true);
// get pods we have not tried to auto add for a while
    $triedPods = R::getCol("
    SELECT value
    FROM meta
    WHERE date_created > now() - interval '72 hours'
    AND name = 'add_attempt'
 ");
    $skipPods    = array_merge($existingPods->domain ?? [], $triedPods ?? []);
    $fetchedPods = collect(c('pod-list-providers'))
        ->flatMap(fn($podListProvider) => extractPodsFromCurl(
            $podListProvider['curl'](),
            $podListProvider['field'] ?? null,
            $podListProvider['column'] ?? null,
            $podListProvider['array'] ?? null
        ))
        ->unique();
    $processPods       = $fetchedPods->diff($skipPods);
    $remotepool        = Pool::create()->concurrency((int) $_SERVER['PROCESS_LIMIT']);
    foreach ($processPods as $domain) {
        if (is_string($domain)) {
            cleanDomain($domain);
            $remotepool
                ->add(new AddServerTask($domain))
                ->catch(function ($exception) use ($domain) {
                    $_SERVER['APP_DEBUG'] && podLog('Failed remote add ' . $exception, $domain, 'warning');
                });
            addMeta('add_attempt', $domain);
            if ($limit) {
                break;
            }
        }
    }
    $remotepool->wait();
    gc_collect_cycles();
    return "Crawl Done";
}

function updateDailyStats($interval = '1')
{
    set_time_limit(0);
    $intervalopposite = $interval - 1;
    $sql = "
    SELECT
        to_char(checks.date_checked, 'yyyy-mm-dd') AS yymmdd,
        (sum(checks.total_users) / count(checks.domain)) as users,
        (sum(checks.active_users_halfyear) / count(checks.domain)) as active_users_halfyear,
        (sum(checks.active_users_monthly) / count(checks.domain)) as active_users_monthly,
        (sum(checks.local_posts) / count(checks.domain)) as posts,
        (sum(checks.comment_counts) / count(checks.domain)) as comments,
        (count(checks.domain) / count(checks.domain)) as pods,
        (count(nullif(checks.online, false)) / count(checks.domain)) as uptime,
        checks.domain,
        servers.softwarename as softwarename
    FROM checks
    INNER JOIN servers ON servers.domain = checks.domain 
    WHERE date_checked >= date_trunc('day', current_date - interval '$interval day') and date_checked < date_trunc('day', current_date - interval '$intervalopposite day')
    AND online = true
    GROUP BY checks.domain,servers.softwarename,yymmdd
";
    $daily_totals_all = R::getAll($sql);
    $rinterval = '-' . $interval . ' day';
    $timestamp = date(('Y-m-d'), strtotime($rinterval)) . ' 01:01:01-01';

//stats for all softwares
    $all_users = 0;
    $all_active_users_halfyear = 0;
    $all_active_users_monthly = 0;
    $all_posts = 0;
    $all_comments = 0;
    $all_pods = 0;

    foreach ($daily_totals_all as $all_daily) {
        $all_users                 = $all_users + $all_daily['users'];
        $all_active_users_halfyear = $all_active_users_halfyear + $all_daily['active_users_halfyear'];
        $all_active_users_monthly  = $all_active_users_monthly + $all_daily['active_users_monthly'];
        $all_posts                 = $all_posts + $all_daily['posts'];
        $all_comments              = $all_comments + $all_daily['comments'];
        $all_pods                  = $all_pods + $all_daily['pods'];
    }

    try {
        $all_p = R::findOrCreate('dailystats', ['date_checked' => $timestamp, 'softwarename' => 'all']);
        $all_p['softwarename']                  = 'all';
        $all_p['total_users']                   = $all_users;
        $all_p['total_active_users_halfyear']   = $all_active_users_halfyear;
        $all_p['total_active_users_monthly']    = $all_active_users_monthly;
        $all_p['total_posts']                   = $all_posts;
        $all_p['total_comments']                = $all_comments;
        $all_p['total_pods']                    = $all_pods;
        $allstore = R::store($all_p);
        podLog('Daily all store to db', $allstore);
    } catch (RedExceptionAlias $e) {
        podLog('Error in SQL query at insert daily for all' . $e->getMessage(), 'all', 'error');
    }

// stats by software
    $resort = groupBy($daily_totals_all, 'softwarename');
    foreach ($resort as $software) {
        $users = 0;
        $active_users_halfyear = 0;
        $active_users_monthly = 0;
        $posts = 0;
        $comments = 0;
        $pods = 0;
        foreach ($software as $daily) {
            $users                 = $users + $daily['users'];
            $active_users_halfyear = $active_users_halfyear + $daily['active_users_halfyear'];
            $active_users_monthly  = $active_users_monthly + $daily['active_users_monthly'];
            $posts                 = $posts + $daily['posts'];
            $comments              = $comments + $daily['comments'];
            $pods                  = $pods + $daily['pods'];
        }
        if ($daily['softwarename']) {
            try {
                $p = R::findOrCreate('dailystats', ['date_checked' => $timestamp, 'softwarename' => $daily['softwarename']]);
                $p['softwarename']                = $daily['softwarename'];
                $p['total_users']                 = $users;
                $p['total_active_users_halfyear'] = $active_users_halfyear;
                $p['total_active_users_monthly']  = $active_users_monthly;
                $p['total_posts']                 = $posts;
                $p['total_comments']              = $comments;
                $p['total_pods']                  = $pods;
                $eachstore = R::store($p);
                podLog('Daily each store to db result:', $eachstore . ' software: ' . $daily['softwarename']);
            } catch (RedExceptionAlias $e) {
                podLog('Error in SQL query at insert monthly for a software' . $e->getMessage(), $daily['softwarename'], 'error');
            }
        }
    }
    return "Daily Done";
}

function updateMonthlyStats()
{
    set_time_limit(0);
    try {
        $total_runs = R::getAll("
        SELECT
            count(value), 
            to_char(date_created, 'yyyy-mm') as yymm
        FROM meta
        WHERE name = 'pods_updating' AND value = '1'
        GROUP BY yymm
    ");
    } catch (RedExceptionAlias $e) {
        podLog('Error in SQL select query' . $e->getMessage(), '', 'error');
    }

    $monthly_totals_all = R::getAll("
    SELECT
        to_char(date_checked, 'yyyy-mm') AS yymm,
        sum(total_users) as users,
        sum(active_users_halfyear) as active_users_halfyear,
        sum(active_users_monthly) as active_users_monthly,
        sum(local_posts) as posts,
        sum(comment_counts) as comments,
        count(domain) as pods,
        count(nullif(online, false)) as uptime
    FROM checks
    WHERE date_checked >= date_trunc('month', current_date - interval '3 month')
    AND online = true 
    GROUP BY yymm;
");

    foreach ($monthly_totals_all as $monthly) {
        // Format date to timestamp.
        $timestamp = $monthly['yymm'] . '-01 01:01:01-01';

        foreach ($total_runs as $runs) {
            if ($monthly['yymm'] === $runs['yymm']) {
                $total = $runs['count'];
                break;
            }
        }
        if ($total == 0) {
            $total = 1;
        }

        try {
            $p = R::findOrCreate('monthlystats', ['date_checked' => $timestamp, 'softwarename' => 'all']);
            $p['softwarename']                  = 'all';
            $p['total_users']                   = round($monthly['users'] / $total);
            $p['total_active_users_halfyear']   = round($monthly['active_users_halfyear'] / $total);
            $p['total_active_users_monthly']    = round($monthly['active_users_monthly'] / $total);
            $p['total_posts']                   = round($monthly['posts'] / $total);
            $p['total_comments']                = round($monthly['comments'] / $total);
            $p['total_pods']                    = round($monthly['pods'] / $total);
            R::store($p);
        } catch (RedExceptionAlias $e) {
            podLog('Error in SQL query at insert monthly for all' . $e->getMessage(), 'all', 'error');
        }
    }

    $monthly_totals_bypod = R::getAll("
   SELECT
        to_char(checks.date_checked, 'yyyy-mm') AS yymm,
        sum(checks.total_users) as users,
        sum(checks.active_users_halfyear) as active_users_halfyear,
        sum(checks.active_users_monthly) as active_users_monthly,
        sum(checks.local_posts) as posts,
        sum(checks.comment_counts) as comments,
        count(checks.domain) as pods,
        count(nullif(checks.online, false)) as uptime, 
        servers.softwarename as softwarename
    FROM checks
    INNER JOIN servers ON servers.domain = checks.domain 
    WHERE date_checked >= date_trunc('month', current_date - interval '3 month')
    AND online = true 
    GROUP BY yymm,servers.softwarename;
");

    foreach ($monthly_totals_bypod as $monthly) {
        // Format date to timestamp.
        $timestamp = $monthly['yymm'] . '-01 01:01:01-01';

        foreach ($total_runs as $runs) {
            if ($monthly['yymm'] === $runs['yymm']) {
                $total = $runs['count'];
                break;
            }
        }
        if ($total == 0) {
            $total = 1;
        }

        try {
            $p = R::findOrCreate('monthlystats', ['date_checked' => $timestamp, 'softwarename' => $monthly['softwarename']]);
            $p['softwarename']                = $monthly['softwarename'];
            $p['total_users']                 = $p['total_users']                  = round($monthly['users'] / $total);
            $p['total_active_users_halfyear'] = $p['total_active_users_halfyear']  = round($monthly['active_users_halfyear'] / $total);
            $p['total_active_users_monthly']  = $p['total_active_users_monthly']   = round($monthly['active_users_monthly'] / $total);
            $p['total_posts']                 = $p['total_posts']                  = round($monthly['posts'] / $total);
            $p['total_comments']              = round($monthly['comments'] / $total);
            $p['total_pods']                  = round($monthly['pods'] / $total);

            R::store($p);
        } catch (RedExceptionAlias $e) {
            podLog('Error in SQL query at insert monthly for a software' . $e->getMessage(), $monthly['softwarename'], 'error');
        }
    }
    return "Monthly Done";
}

/**
 * Create RSS feeds
 */
function updateRSS(): string
{
    $feed = new Feed();
    $feed->setTitle($_SERVER['TITLE']);
    $feed->setLink('https://' . $_SERVER['DOMAIN'] . '/');
    $feed->setFeedLink('https://' . $_SERVER['DOMAIN'] . '/' . $_SERVER['NEW_SERVERS_RSS'], 'atom');
    $feed->addAuthor([
        'name' => $_SERVER['TITLE'],
        'email' => $_SERVER['ADMIN_EMAIL'],
        'uri' => 'https://' . $_SERVER['DOMAIN'],
    ]);
    $feed->setDateModified(time());
    $feed->addHub('https://pubsubhubbub.appspot.com/');

    $servers = allServersList(null, true, $_SERVER['NEW_SERVERS_RSS_HOURS'] ?? 2);

    foreach ($servers as $server) {
        $entry = $feed->createEntry();
        $entry->setTitle(!empty($server['softwarename']) ? $server['softwarename'] : 'Unknown');
        $entry->setLink('https://' . $_SERVER['DOMAIN'] . '/' . $server['domain']);
        $entry->setDateModified(time());
        $entry->setDateCreated(time());
        $entry->setDescription(!empty($server['metatitle']) ? $server['metatitle'] : 'Unknown');
        $entry->setContent('New ' . $server['softwarename'] . ' server found in the fediverse. '  . 'https://' . $_SERVER['DOMAIN'] . '/' . $server['domain']);
        $feed->addEntry($entry);
    }

    return $feed->export('atom');
}

/**
 * update one server
 */
function updateServer(string $domain): bool
{
    $pool = Pool::create()->concurrency((int) $_SERVER['PROCESS_LIMIT']);
    $pool
        ->add(new UpdateServerTask($domain))
        ->catch(fn() => false);
    $pool->forceSynchronous()->wait();
    return true;
}
/**
 * Create sitemap
 */
function updateSitemap(): void
{
    $config = new Config();
    $config->setBaseURL('https://' . $_SERVER['DOMAIN']);
    $config->setSaveDirectory($_SERVER['BASE_DIR']);
    $generator = new SitemapGenerator($config);
    $generator->setMaxUrlsPerSitemap(50000);
    $generator->setSitemapFileName("sitemap.xml");
    $generator->setSitemapIndexFileName("sitemap-index.xml");
    $pods = allDomainsData() ?? [];
    foreach (array_column($pods, 'domain') as $domain) {
        $generator->addURL('/' . $domain, new DateTime(), 'always', 0.5);
    }
    $generator->flush();
    $generator->finalize();
    $generator->updateRobots();
}
