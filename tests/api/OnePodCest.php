<?php

class OnePodCest
{
    public function _before(ApiTester $I)
    {
    }

    public function seeOne(\ApiTester $I)
    {
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendPost('/', ['query' => '{node(domain:"diasp.org"){domain}}']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"data":{"node":[{"domain":"diasp.org"}]}}');
    }

    public function seeOneFail(\ApiTester $I)
    {
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendPost('/', ['query' => '{node(domain:"xerox.com"){domain}}']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"data":{"node":[]}}');
    }

    public function seeAll(\ApiTester $I)
    {
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendPost('/', ['query' => '{nodes{domain ip}}']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('"domain":"diasp.org"');
    }

    public function seeListbySoftware(\ApiTester $I)
    {
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendPost('/', ['query' => '{nodes(status:"UP",softwarename:"diaspora"){domain}}']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('"domain":"diasp.org"');
    }

    public function seeChecks(\ApiTester $I)
    {
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendPost('/', ['query' => '{checks{domain}}']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('"domain":"diasp.org"');
    }

    public function seeClicks(\ApiTester $I)
    {
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendPost('/', ['query' => '{clicks{domain}}']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        //should not exists since was a non-human click
        $I->cantSeeResponseContains('"domain":"diasp.org"');
    }
    public function seeMonthlyStats(\ApiTester $I)
    {
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendPost('/', ['query' => '{monthlystats{total_pods softwarename}}']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('"softwarename":"diaspora"');
    }

    public function seeDailyStats(\ApiTester $I)
    {
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendPost('/', ['query' => '{dailystats{total_pods softwarename}}']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('"softwarename":"diaspora"');
    }
    public function seeMasterversions(\ApiTester $I)
    {
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendPost('/', ['query' => '{masterversions{software}}']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('"software":"diaspora"');
    }
    public function seeSoftwares(\ApiTester $I)
    {
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendPost('/', ['query' => '{softwares{count softwarename}}']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('"softwarename":"diaspora"');
    }
}
