<?php

require_once __DIR__ . '/../../boot.php';

$software = !empty($subdomain) ? $subdomain : '';

if (!empty($subdomain)) {
    $smallwrapper = 'smallnone';
    $bigwrapper   = 'bigwrapper';
    $hidden       = 'hidden';
} else {
    $smallwrapper = 'smallwrapper';
    $bigwrapper   = 'bignone';
    $hidden       = '';
}

?>
<div class="container">
    <div class="d-flex justify-content-center p-3"><h1><b class="fw-bold text-blue"><?php echo $t->trans('welcome.main.find') ?></b></h1></div>
    <div class="shadow-lg p-3 mb-5 bg-body rounded">
    <div class="d-flex justify-content-center p-3"><h4><strong><?php echo $t->trans('welcome.main.suggested', ['%(software)' => !empty($subdomain) ? $subdomain : ''])?></strong></h4></div>
    <div>
        <?php
        $closestservers = closestServers($_SERVER['REMOTEADDR'] ?? $_SERVER['REMOTE_ADDR'], 6, !empty($subdomain) ? $subdomain : '%');
        if (count($closestservers) > 0) {
            foreach ($closestservers as $server) {
                $location  = $server['city'] ?? $server['state'] ?? $server['countryname'];
                $server['greenhost'] ? $textcolor = ' text-success' : $textcolor = ' text-brown';
                echo '<div class="d-flex justify-content-md-center"><a class="fw-bold p-2' . $textcolor . '" target="go" href="/go&domain=' . $server['domain'] . '">';
                echo $t->trans('welcome.main.picked', ['%(domain)' => $server['domain'], '%(software)' => $server['softwarename'], '%(location)' => $location]);
                echo '</a><a href="/' . $server['domain'] . '"><img src="app/assets/images/info.svg" alt="Server Details" width="16" height="16"></a></div>';
            }
        } else {
            $location = ipLocation($_SERVER['REMOTEADDR'] ?? $_SERVER['REMOTE_ADDR']);
            echo '<div class="d-flex justify-content-md-center fw-bold text-brown">';
            echo $t->trans('welcome.main.nopicked', ['%(software)' => !empty($subdomain) ? $subdomain : '', '%(location)' => $location['city']]);
            echo '</div>';
        }
        ?>
    </div>
    </div>
    <div class="d-flex justify-content-center pb-3"><h3><?php echo $t->trans('welcome.main.first') ?></h3></div>
            <div class="row row-cols-auto justify-content-center">
                <?php
                $softwares = c('softwares');
                foreach ($softwares as $soft => $welcome_item) {
                    if (isset($welcome_item['welcome']) && $welcome_item['welcome'] === '1') {
                        if ($welcome_item['text'] == $software) {
                            $iconselector = 'iconselected';
                        } else {
                            $iconselector = 'iconnotselected';
                        }
                        if ($_SERVER['CDN_DOMAIN']) {
                            printf(
                                '<div class="p-2 m-3 %5$s %9$s"><div class="fa-ani"><a href="//%2$s.%6$s%7$s"><img height="72" width="72" data-bs-custom-class="custom-tooltip" data-bs-toggle="tooltip" data-bs-placement="right" title="%10$s" alt="%10$s" src="%11$s%4$s" class="%5$s %9$s" aria-hidden="true"></a><div class="text-center text-muted %8$s">%3$s</div></div></div>',
                                '',
                                $welcome_item['href'],
                                $welcome_item['text'],
                                $welcome_item['icon'],
                                $smallwrapper,
                                $_SERVER['DOMAIN'],
                                $_SERVER['REQUEST_URI'],
                                $hidden,
                                $iconselector,
                                $t->trans('softwares.' . $welcome_item['text']),
                                $_SERVER['CDN_DOMAIN']
                            );
                        } else {
                            printf(
                                '<div class="p-2 m-3 %5$s %9$s"><div class="fa-ani"><a href="//%2$s.%6$s%7$s"><img height="72" width="72" data-bs-toggle="tooltip" data-bs-custom-class="custom-tooltip" data-bs-placement="right" title="%10$s" alt="%10$s" src="%4$s" class="%5$s %9$s" aria-hidden="true"></a><div class="text-center text-muted %8$s">%3$s</div></div></div>',
                                '',
                                $welcome_item['href'],
                                $welcome_item['text'],
                                $welcome_item['icon'],
                                $smallwrapper,
                                $_SERVER['DOMAIN'],
                                $_SERVER['REQUEST_URI'],
                                $hidden,
                                $iconselector,
                                $t->trans('softwares.' . $welcome_item['text'])
                            );
                        }
                    }
                }
                ?>
            </div>
<br>
    <div class="d-flex justify-content-center p-2"><h3><?php echo $t->trans('welcome.main.second') ?></h3></div>
        <div class="d-lg-flex justify-content-center">
            <div class="<?php echo $bigwrapper ?> p-4">
            <div class="col-sm-auto fa-ani">
                <a href="/map"><img src="<?php echo $_SERVER['CDN_DOMAIN'] ?>app/assets/images/map.svg" alt="Map View" width="48" height="48"></a>
            </div>
                <br><?php echo $t->trans('welcome.main.map') ?>
            </div>
            <div class="<?php echo $bigwrapper ?> p-4">
            <div class="col-sm-auto fa-ani">
                <a href="/list"><img src="<?php echo $_SERVER['CDN_DOMAIN'] ?>app/assets/images/list-columns.svg" alt="List View" width="48" height="48"></a>
            </div>
                <br><?php echo $t->trans('welcome.main.list') ?>
        </div>
    </div>
</div>
<div class="pb-lg-5 mb-5"></div>
