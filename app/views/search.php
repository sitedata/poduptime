<?php

/**
 * get data and display for search query.
 */

declare(strict_types=1);

use Matriphe\ISO639\ISO639;
use Nette\Utils\Paginator;
use Poduptime\PodStatus;
use RedBeanPHP\R;
use RedBeanPHP\RedException;

require_once __DIR__ . '/../../boot.php';

$iso             = new ISO639();
$hiddensoftwares = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
$hiddendomains   = txtToQuery($_SERVER['DOMAINS_BLACKLIST']);

$urlArr          = parse_url($_SERVER['REQUEST_URI']);
if (strlen($urlArr['query']) <= 6) {
    echo 'No search query<br>';
} else {
    parse_str($urlArr['query'], $output);
    $query = strip_tags(mb_convert_encoding($output['query'], 'UTF-8', 'ISO-8859-1'));
}

$limit     = 10;
$tmp       = 0;
$paginator = new Paginator();
isset($output['page']) ? ($page = (int) $output['page']) : ($page = 0);
$paginator->setPage($page);
$paginator->setItemsPerPage(10);
$offset    = $paginator->getOffset();

if ($query) {
    try {
        $pods = R::getAll('
        SELECT domain, softwarename, signup, name, detectedlanguage, status, metatitle, metadescription, metalocation, zipcode, countryname, greenhost
        FROM servers
        WHERE status = :PodStatus
          AND softwarename NOT SIMILAR TO :hiddensoftwares
          AND domain NOT SIMILAR TO :hiddendomains
          AND (
              (lower(domain) LIKE :query)
              OR (lower(metatitle) LIKE :query)
              OR (lower(metadescription) LIKE :query)
              OR (lower(metalocation) LIKE :query)
              OR (lower(zipcode) LIKE :zipquery)
              )
        ORDER BY score DESC
        OFFSET :offset
    ', [':PodStatus' => PodStatus::UP, ':zipquery' => substr(strtolower($query), 0, 2) . '%', ':query' => '%' . strtolower($query) . '%', ':hiddensoftwares' => $hiddensoftwares, ':hiddendomains' => $hiddendomains, ':offset' => $offset]);
    } catch (RedException $e) {
        die('Error in SQL query: ' . $e->getMessage());
    }
}

$paginator->setItemCount(count($pods));

if ($pods) {
    echo '<div class="container-fluid">';
    echo '<div class="col m-lg-2 pb-2 fw-bold">';
    if ($page > 1) {
        echo ' <a href="/search?query=' . $query . '&page=' . $paginator->getPage() - 1 . '">' . $t->trans('base.strings.search.lastpage') . '</a> ';
    }
    echo $t->trans('base.strings.search.results', ['%(page)' => $paginator->getPage(), '%(pages)' => $paginator->getPageCount(), '%(number)' => count($pods), '%(searchterm)' => $query]);
    if (count($pods) > $limit) {
        echo ' <a href="/search?query=' . $query . '&page=' . $paginator->getPage() + 1 . '">' . $t->trans('base.strings.search.nextpage') . '</a> ';
    }
    echo '</div>';
    foreach ($pods as $pod) {
        if ($tmp++ < 10) {
            echo '<div class="shadow-lg p-1 mb-3 bg-body rounded"><div class="row ps-1 ms-2 pt-2">';
            echo '<div class="col small"><a class="text-blue" target="_pod" href="/go&domain=' . $pod['domain'] . '">https://' . idn_to_utf8($pod['domain']) . '</a><a class="d-inline-block small ps-2 mb-1" href="/' . $pod['domain'] . '"><img src="' . $_SERVER["CDN_DOMAIN"] . 'app/assets/images/info.svg" data-bs-toggle="tooltip" data-bs-custom-class="custom-tooltip" data-bs-placement="right" title="' . $t->trans('base.general.moreinfo') . '" width="22" height="22"></a></div>';
            echo '</div><div class="row ps-1 ms-2">';
            echo '<div class="col text-primary h4"><a class="" target="_pod" href="/go&domain=' . $pod['domain'] . '">' . $pod['metatitle'] . '</a></div>';
            echo '</div><div class="row ps-1 ms-2">';
            echo '<div class="col text-brown">' . $pod['metadescription'] . '</div>';
            echo '</div><div class="row col-lg-auto p-1 m-1 text-secondary small">';
            echo '<div class="col">' . $t->trans('base.strings.search.software') . ': <b class="text-blue">' . $pod['softwarename'] . '</b><img src="' . $_SERVER["CDN_DOMAIN"] . 'app/assets/images/chevron-double-right.svg" class="m-1" alt="Chevron Right" width="12" height="12">';
            echo $t->trans('base.strings.search.open') . ': <b class="text-blue">' . ($pod['signup'] ? $t->trans('base.general.yes') : $t->trans('base.general.no')) . '</b><img src="' . $_SERVER["CDN_DOMAIN"] . 'app/assets/images/chevron-double-right.svg" class="m-1" alt="Chevron Right" width="12" height="12">';
            echo $t->trans('base.strings.search.location') . ': <b class="text-blue">' . $pod['countryname'] . '</b><img src="' . $_SERVER["CDN_DOMAIN"] . 'app/assets/images/chevron-double-right.svg" class="m-1" alt="Chevron Right" width="12" height="12">';
            echo $t->trans('base.strings.search.greenhost') . ': <b class="text-success">' . ($pod['greenhost'] ? 'Yes' : 'No') . '</b><img src="' . $_SERVER["CDN_DOMAIN"] . 'app/assets/images/chevron-double-right.svg" class="m-1" alt="Chevron Right" width="12" height="12">';
            echo $t->trans('base.strings.search.language') . ': <b class="text-blue">' . ($pod['detectedlanguage'] ? $iso->languageByCode1($pod['detectedlanguage']) : '') . '</b></div>';
            echo '<div class="mb-1"></div>';
            echo '</div></div>';
        }
    }
    echo '</div>';
    podLog('Search for ' . $query);
} else {
    echo 'No search results<br>';
}
