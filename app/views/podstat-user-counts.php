<?php

/**
 * Show detailed pod stats.
 */

declare(strict_types=1);

// Required parameters.
($_domain = $_GET['domain'] ?? null) || die('no domain given');

require_once __DIR__ . '/../../boot.php';

$totals = oneDomainsChecks($_domain);

?>
<div class="chart-container p-1 d-flex w-100">
    <canvas class="d-flex w-100" id="pod_chart_user_counts"></canvas>
</div>
<script>
    /**
     * Add a new chart for the passed data.
     *
     * @param id   HTML element ID to place the chart.
     * @param data Data to display on the chart.
     */
    new Chart(document.getElementById('pod_chart_user_counts'), {
        type: "line",
        data: {
            labels: <?php echo json_encode(array_column($totals, 'yymm')); ?>,
            datasets: [{
                data: <?php echo json_encode(array_column($totals, 'users')); ?>,
                label: '<?php echo $t->trans('base.general.users') ?>',
                fill: false,
                yAxisID: "l2",
                borderColor: "#A07614",
                backgroundColor: "#A07614",
                borderWidth: 4,
                pointHoverRadius: 6
            }, {
                data: <?php echo json_encode(array_column($totals, 'active_users_halfyear')); ?>,
                label: '<?php echo $t->trans('base.strings.stats.halfyear') ?>',
                fill: false,
                yAxisID: "l2",
                borderColor: "#4b6588",
                backgroundColor: "#4b6588",
                borderWidth: 4,
                pointHoverRadius: 6
            }, {
                data: <?php echo json_encode(array_column($totals, 'active_users_monthly')); ?>,
                label: '<?php echo $t->trans('base.strings.stats.monthly') ?>',
                fill: false,
                yAxisID: "l2",
                borderColor: "#cecaa7",
                backgroundColor: "#cecaa7",
                borderWidth: 4,
                pointHoverRadius: 6
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: true,
            scales: {
                l2: {
                    position: "left"
                },
                x: {
                    ticks: {
                        maxRotation: 90,
                        minRotation: 90
                    }
                }
            },
            interaction: {
                intersect: false,
                mode: 'index',
            },
        }
    });
</script>
