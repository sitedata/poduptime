<?php

/**
 * Show pod list table.
 */

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

?>

<table id="table"
       data-toggle="table"
       data-pagination="true"
       data-ajax="ajaxRequest"
       data-show-toggle="true"
       data-show-fullscreen="true"
       data-show-columns="true"
       data-row-style="rowStyle"
       data-show-refresh="true"
       data-show-columns-toggle-all="true"
       data-detail-view="true"
       data-detail-view-by-click="true"
       data-detail-view-icon="false"
       data-filter-control="true"
       data-detail-formatter="detailFormatter"
       data-page-size="35"
       data-loading-template="loadingTemplate"
       data-page-list="[100, 250, 500, 1000, all]"
>
    <thead>
    <tr>
        <th data-sortable="true" data-filter-control="input" data-field="domain"><?php echo $t->trans('base.strings.list.columns.server') ?></th>
        <th data-sortable="true" data-filter-control="input" data-field="uptime_alltime"><?php echo $t->trans('base.strings.list.columns.uptime') ?></th>
        <th data-sortable="true" data-filter-control="input" data-field="signup"><?php echo $t->trans('base.strings.list.columns.signups') ?></th>
        <th data-sortable="true" data-filter-control="input" data-field="active_users_monthly" title="<?php echo $t->trans('base.strings.list.columns.users1desc') ?>"><?php echo $t->trans('base.strings.list.columns.activeusers') ?></th>
        <th data-sortable="true" data-filter-control="input" data-field="total_users" title="<?php echo $t->trans('base.strings.list.columns.usersdesc') ?>"><?php echo $t->trans('base.strings.list.columns.users') ?></th>
        <th data-sortable="true" data-filter-control="input" data-field="countryname"><?php echo $t->trans('base.strings.list.columns.country') ?></th>
        <th data-sortable="true" data-visible="false" data-filter-control="input" data-field="greenhost"><?php echo $t->trans('base.strings.list.columns.greenhost') ?></th>
        <th data-sortable="true" data-visible="false" data-filter-control="input" data-field="softwarename"><?php echo $t->trans('base.strings.list.columns.software') ?></th>
        <th data-sortable="true" data-visible="false" data-filter-control="input" data-field="shortversion"><?php echo $t->trans('base.strings.list.columns.version') ?></th>
        <th data-sortable="true" data-visible="false" data-filter-control="input" data-field="comment_counts"><?php echo $t->trans('base.strings.list.columns.comments') ?></th>
        <th data-sortable="true" data-visible="false" data-filter-control="input" data-field="local_posts"><?php echo $t->trans('base.strings.list.columns.posts') ?></th>
        <th data-sortable="true" data-visible="false" data-filter-control="input" data-field="monthsmonitored"><?php echo $t->trans('base.strings.list.columns.months') ?></th>
        <th data-sortable="true" data-filter-control="input" data-field="detectedlanguage"><?php echo $t->trans('base.strings.list.columns.language') ?></th>
    </tr>
    </thead>
</table>
<script>
    var apiurl = $('input:input[name="apiurl"]').val();
    let host = window.location.host
    function ajaxRequest(params) {
        if (host.split('.').length > 2) {
            var subdomain = host.split('.')[0]
        } else {
            var subdomain = ''
        }
        $.ajax({
            url: apiurl,
            method: 'POST',
            data: JSON.stringify({'query':'{nodes(softwarename:\"' + subdomain + '\" status: \"UP\"){domain uptime_alltime signup total_users active_users_monthly countryname greenhost detectedlanguage name softwarename shortversion comment_counts local_posts monthsmonitored}}'}),
            success: function (json) {
                params.success(json.data.nodes)
            }
        });
    }
    function detailFormatter(index, row) {
        let bob = ''
        if (row.greenhost === true)  {bob += '<div class="text-success mb-3"><?php echo htmlspecialchars($t->trans('base.strings.list.columns.greenhostdesc')) ?></div>'}
        bob += '<div class="text-brown"><a target="_pod" class="text-brown text-decoration-none" href=//' + host + '/go&domain=' + row.domain + '>'
        + '<?php echo htmlspecialchars($t->trans('base.general.visitserver')) ?> ' + row.softwarename + ' <?php echo htmlspecialchars($t->trans('base.general.server')) ?> ' + row.domain + '</a></div>'
        + '<br><div class="text-brown"><a class="text-brown text-decoration-none" href=//' + host + '/' + row.domain + '><?php echo htmlspecialchars($t->trans('base.general.moreinfo')) ?></a></div>'
        return bob
    }
    function loadingTemplate(message) {
            return '<div class="spin spinner-border" role="status"><div class="sr-only"></div></div>'
    }
    function rowStyle(row, index) {
        if (index % 2 === 0) {
            return {
                css: {
                    background: '#fafafa'
                }
            }
        }
        return {
            css: {
                background: '#dddddd'
            }
        }
    }

</script>