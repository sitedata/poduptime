$(document).ready(function () {
    var domain = $('input:input[name="domain"]').val();
    var apiurl = $('input:input[name="apiurl"]').val();
    $('#jsonrawclick').one('click', function () {
        let query = JSON.stringify({"query":"{node(domain: \"" + domain + "\"){id name metatitle metadescription detectedlanguage metaimage owner onion i2p ip ipv6 greenhost host dnssec sslexpire servertype camo terms pp support softwarename shortversion fullversion masterversion daysmonitored monthsmonitored date_updated date_laststats date_created metalocation country city state zipcode countryname lat long uptime_alltime latency sslexpire total_users active_users_monthly active_users_halfyear local_posts comment_counts score status signup podmin_statement services protocols}}"})
        $('#jsonrawquery').append('<b>' + query + '</b><br>');
        $.ajax({
            type: 'POST',
            url: apiurl,
            async: true,
            data: query,
            dataType: 'json',
            success: function (json) {
                var jsonPretty = JSON.stringify(json.data.node, null, ' ');
                $('#jsonrawresponse').append(jsonPretty).fadeTo(2450, 0.90);
                $('#jsonrawquery').fadeTo(2450, 0.20);
            },
            error: function (e) {
                alert(e.message);
            }
        });
    });
    $('#jsonmetaclick').one('click', function () {
        let query = JSON.stringify({"query":"{node(domain: \"" + domain + "\"){additional_meta greenwebdata}}"})
        $('#jsonmetaquery').append('<b>' + query + '</b><br>');
        $.ajax({
            type: 'POST',
            url: apiurl,
            async: true,
            data: query,
            dataType: 'json',
            success: function (json) {
                var jsonPretty = JSON.stringify(JSON.parse(json.data.node[0]['additional_meta']), null, ' ');
                jsonPretty += JSON.stringify(JSON.parse(json.data.node[0]['greenwebdata']), null, ' ');
                $('#jsonmetaresponse').append(jsonPretty).fadeTo(2450, 0.90);
                $('#jsonmetaquery').fadeTo(2450, 0.20);
            },
            error: function (e) {
                alert(e.message);
            }
        });
    });
    $('#jsonerrorclick').one('click', function () {
        let query = JSON.stringify({"query":"{checks(online:\"false\",domain:\"" + domain + "\",limit:50){date_checked error online}}"})
        $('#jsonerrorquery').append('<b>' + query + '</b><br>');
        $.ajax({
            type: 'POST',
            url: apiurl,
            async: true,
            data: query,
            dataType: 'json',
            success: function (json) {
                var jsonPretty = JSON.stringify(json.data.checks, null, ' ');
                $('#jsonerrorresponse').append(jsonPretty).fadeTo(2450, 0.90);
                $('#jsonerrorquery').fadeTo(2450, 0.20);
            },
            error: function (e) {
                alert(e.message);
            }
        });
    });
});

