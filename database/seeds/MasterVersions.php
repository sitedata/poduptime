<?php

declare(strict_types=1);

use Phinx\Seed\AbstractSeed;

class MasterVersions extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('masterversions');
        $table
            ->addColumn('software', 'text')
            ->addColumn('version', 'text')
            ->addColumn('devlastcommit', 'timestamp')
            ->addColumn('releasedate', 'timestamp')
            ->addColumn('date_checked', 'timestamp', ['default' => 'current_timestamp'])
            ->create();
    }
}
