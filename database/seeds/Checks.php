<?php

declare(strict_types=1);

use Phinx\Seed\AbstractSeed;

class Checks extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('checks');
        $table
            ->addColumn('domain', 'text')
            ->addColumn('online', 'boolean')
            ->addColumn('error', 'text')
            ->addColumn('latency', 'decimal', ['precision' => 8, 'scale' => 6])
            ->addColumn('total_users', 'integer')
            ->addColumn('active_users_halfyear', 'integer')
            ->addColumn('active_users_monthly', 'integer')
            ->addColumn('local_posts', 'integer')
            ->addColumn('comment_counts', 'integer')
            ->addColumn('shortversion', 'text')
            ->addColumn('version', 'text')
            ->addColumn('date_checked', 'timestamp', ['default' => 'current_timestamp'])
            ->create();
    }
}
