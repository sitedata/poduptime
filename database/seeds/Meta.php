<?php

declare(strict_types=1);

use Phinx\Seed\AbstractSeed;

class Meta extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('meta');
        $table
            ->addColumn('name', 'text')
            ->addColumn('value', 'text')
            ->addColumn('date_created', 'timestamp', ['default' => 'current_timestamp'])
            ->create();
    }
}
