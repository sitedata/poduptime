<?php

/**
 * Main API welcome screen.
 */

declare(strict_types=1);

require_once __DIR__ . '/../boot.php';

?>
<html>
  <head>
    <title><?php echo $_SERVER['TITLE'] ?> | GraphiQL API</title>
    <link href="/node_modules/graphiql/graphiql.min.css" rel="stylesheet" />
  </head>
  <body style="margin: 0;">
    <div id="graphiql"></div>
    <script
      src="/node_modules/react/umd/react.production.min.js"
    ></script>
    <script
      src="/node_modules/react-dom/umd/react-dom.production.min.js"
    ></script>
    <script
      src="/node_modules/graphiql/graphiql.min.js"
    ></script>

    <script>
const customQuery =
`
# Welcome to GraphiQL
#
# GraphiQL is an in-browser tool for writing, validating, and
# testing GraphQL queries.
#
# Type queries into this side of the screen, and you will see intelligent
# typeaheads aware of the current GraphQL type schema and live syntax and
# validation errors highlighted within the text.
#
# GraphQL queries typically start with a "{" character. Lines that start
# with a # are ignored.
#
# An example GraphQL query might look like:
#
#     {
#       field(arg: "value") {
#         subField
#       }
#     }
#
# Keyboard shortcuts:
#
#   Prettify query:  Shift-Ctrl-P (or press the prettify button)
#
#  Merge fragments:  Shift-Ctrl-M (or press the merge button)
#
#        Run Query:  Ctrl-Enter (or press the play button)
#
#    Auto Complete:  Ctrl-Space (or just start typing)
#
# Documentation is in left menu bar
# Some examples at: https://gitlab.com/diasporg/poduptime/-/wikis/API
`;
      const graphQLFetcher = graphQLParams =>
        fetch('', {
          method: 'post',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(graphQLParams),
        })
          .then(response => response.json())
          .catch(() => response.text());
      ReactDOM.render(
        React.createElement(GraphiQL, { fetcher: graphQLFetcher, defaultQuery: customQuery }),
        document.getElementById('graphiql'),
      );
    </script>
  </body>
</html>
